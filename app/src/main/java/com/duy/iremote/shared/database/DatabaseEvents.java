package com.duy.iremote.shared.database;

public class DatabaseEvents {
    public static final String NEW_DEVICE_ADDED = "com.duy.iremote.shared.database.NEW_DEVICE_ADDED";
    public static final String DEVICE_REMOVED = "com.duy.iremote.shared.database.DEVICE_REMOVED";
    public static final String DEVICE_CHANGED = "com.duy.iremote.shared.database.DEVICE_CHANGED";

    public static final String NEW_CONFIG_ADDED = "com.duy.iremote.shared.database.NEW_CONFIG_ADDED";
    public static final String CONFIG_CHANGED = "com.duy.iremote.shared.database.CONFIG_CHANGED";
    public static final String CONFIG_REMOVED = "com.duy.iremote.shared.database.CONFIG_REMOVED";
}
