package com.duy.iremote.shared.config.input;

import android.content.Context;

import com.duy.iremote.shared.config.ExpressionContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public interface Input {
    double getValue(ExpressionContext context);

    void writeToXml(Document rootDocument, Element output);

    CharSequence toReadableString(Context context);
}
