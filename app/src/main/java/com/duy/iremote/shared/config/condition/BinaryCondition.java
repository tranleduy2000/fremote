package com.duy.iremote.shared.config.condition;

import android.support.annotation.NonNull;

import com.duy.iremote.shared.config.ICondition;
import com.duy.iremote.shared.config.input.Input;

public interface BinaryCondition extends ICondition {

    @NonNull
    Input getLeftNode();

    @NonNull
    Input getRightNode();
}
