package com.duy.iremote.shared.config;

import android.content.Context;
import android.support.annotation.Nullable;

import com.duy.iremote.shared.database.IFirebaseObject;

import java.util.HashMap;
import java.util.List;

public interface IConfiguration extends IFirebaseObject, FirebaseExportable {

    void addCondition(ICondition condition);

    @Nullable
    String getName();

    void check(HashMap<String, Double> sensorValues);

    List<ICondition> getConditions();

    CharSequence toReadableString(Context context);
}
