package com.duy.iremote.shared.database;

import android.support.annotation.NonNull;

public interface IFirebaseObject {
    @NonNull
    abstract String getIdentifier();
}
