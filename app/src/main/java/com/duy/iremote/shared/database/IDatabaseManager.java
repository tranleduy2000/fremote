package com.duy.iremote.shared.database;

public interface IDatabaseManager extends
        IDeviceManager,
        IConfigurationManager,
        ISensorValueManager {

}
