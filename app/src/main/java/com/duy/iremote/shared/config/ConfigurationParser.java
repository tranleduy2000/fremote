package com.duy.iremote.shared.config;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.duy.iremote.shared.config.command.ICommand;
import com.duy.iremote.shared.config.command.SetPinValueCommand;
import com.duy.iremote.shared.config.condition.SensorCondition;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConfigurationParser {

    public static ICommand parseCommand(Map<String, Object> properties) {
        if (!properties.containsKey(ICommand.KEY_COMMAND_TYPE)) {
            return null;
        }
        String type = (String) properties.get(ICommand.KEY_COMMAND_TYPE);
        switch (type) {
            case SetPinValueCommand.TYPE:
                return new SetPinValueCommand(properties);
        }
        return null;
    }

    @NonNull
    public static List<ICondition> parseConditions(List<Map<String, Object>> conditionProperties) {
        List<ICondition> conditions = new ArrayList<>();
        for (Map<String, Object> conditionProperty : conditionProperties) {
            ICondition condition = parseCondition(conditionProperty);
            if (condition != null) {
                conditions.add(condition);
            }
        }
        return conditions;
    }

    @Nullable
    private static ICondition parseCondition(Map<String, Object> conditionProperty) {
        if (!conditionProperty.containsKey(ICondition.KEY_CONDITION_TYPE)) {
            return null;
        }
        String type = String.valueOf(conditionProperty.get(ICondition.KEY_CONDITION_TYPE));
        switch (type) {
            case SensorCondition.SENSOR_CONDITION_TYPE:
                return new SensorCondition(conditionProperty);
        }
        return null;
    }

    @Nullable
    public Configuration parse(Map<String, Object> map) {
        Configuration configuration = new Configuration(map);

        return configuration;
    }
}
