package com.duy.iremote.shared.config.condition;

import android.content.Context;
import android.support.annotation.NonNull;

import com.duy.iremote.shared.config.ExpressionContext;
import com.duy.iremote.shared.config.input.Input;

import java.util.Map;


public class RelationOperatorCondition implements BinaryCondition {

    private static final String LEFT_NODE_TAG = "left_node";
    private static final String RIGHT_NODE_TAG = "right_node";
    private static final String OPERATOR_TAG = "operator";
    @NonNull
    private RelationOperator relation;
    @NonNull
    private Input leftNode;
    @NonNull
    private Input rightNode;

    public RelationOperatorCondition(@NonNull RelationOperator relation, @NonNull Input leftNode, @NonNull Input rightNode) {
        this.relation = relation;
        this.leftNode = leftNode;
        this.rightNode = rightNode;
    }

    @Override
    public boolean test(ExpressionContext context) {
        double leftValue = getLeftNode().getValue(context);
        double rightValue = getRightNode().getValue(context);
        switch (relation) {
            case LESS:
                return (leftValue < rightValue);
            case EQUAL:
                return (leftValue == rightValue);
            case GREATER:
                return (leftValue > rightValue);
            case LESS_EQUAL:
                return (leftValue <= rightValue);
            case GREATER_EQUAL:
                return (leftValue >= rightValue);
            default:
                throw new UnsupportedOperationException(relation.name());
        }
    }

    @Override
    public CharSequence toReadableString(Context context) {
        return leftNode.toReadableString(context) + " " + relation.getDisplaySymbol() + " " + rightNode.toReadableString(context);
    }

    @NonNull
    @Override
    public final Input getLeftNode() {
        return leftNode;
    }

    @NonNull
    @Override
    public final Input getRightNode() {
        return rightNode;
    }

    @NonNull
    public RelationOperator getRelation() {
        return relation;
    }

    @Override
    public Map<String, Object> toMap() {
        throw new UnsupportedOperationException();
    }

    public enum RelationOperator {
        GREATER(">"), GREATER_EQUAL(">="), LESS("<"), LESS_EQUAL("<="), EQUAL("=");

        private String displaySymbol;

        RelationOperator(String displaySymbol) {
            this.displaySymbol = displaySymbol;
        }

        public String getDisplaySymbol() {
            return displaySymbol;
        }
    }
}
