package com.duy.iremote.shared.config;

import java.util.HashMap;

public class ExpressionContext {
    private HashMap<String, Double> variables = new HashMap<>();

    public ExpressionContext(HashMap<String, Double> sensorValues) {
        this.variables = sensorValues;
    }

    public double getVariable(String name) {
        Double value = variables.get(name);
        if (value == null) {
            return -1;
        }
        return value;
    }

    public void setVariable(String name, double value) {
        variables.put(name, value);
    }
}
