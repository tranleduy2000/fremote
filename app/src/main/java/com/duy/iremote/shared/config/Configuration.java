package com.duy.iremote.shared.config;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.duy.iremote.shared.config.command.ICommand;
import com.duy.iremote.shared.config.command.SetPinValueCommand;
import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Configuration implements IConfiguration, Cloneable {

    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_CONDITIONS = "conditions";
    public static final String KEY_COMMAND = "command";

    private long identifier;
    @Nullable
    private String name;
    @NonNull
    private List<ICondition> conditions = new ArrayList<>();
    @Nullable
    private ICommand command;

    public Configuration() {
        this.identifier = System.currentTimeMillis();
    }

    public Configuration(@NonNull Map<String, Object> map) {
        if (map.containsKey(KEY_ID)) {
            this.identifier = Long.valueOf(String.valueOf(map.get(KEY_ID)));
        }
        if (map.containsKey(Configuration.KEY_NAME)) {
            this.name = ((String) map.get(Configuration.KEY_NAME));
        }
        if (map.containsKey(Configuration.KEY_CONDITIONS)) {
            this.conditions = (ConfigurationParser.parseConditions((List<Map<String, Object>>) map.get(Configuration.KEY_CONDITIONS)));
        }
        if (map.containsKey(Configuration.KEY_COMMAND)) {
            this.command = (ConfigurationParser.parseCommand((Map<String, Object>) map.get(Configuration.KEY_COMMAND)));
        }
    }

    @Nullable
    public SetPinValueCommand getCommand() {
        return (SetPinValueCommand) command;
    }

    public void setCommand(@Nullable ICommand command) {
        this.command = command;
    }

    @Override
    public void addCondition(ICondition condition) {
        this.conditions.add(condition);
    }

    @Nullable
    @Override
    public String getName() {
        return name;
    }

    @Override
    public void check(HashMap<String, Double> sensorValues) {
        if (conditions.isEmpty()) {
            return;
        }
        for (ICondition condition : conditions) {
            if (!condition.test(new ExpressionContext(sensorValues))) {
                return;
            }
        }
        if (command != null) {
            command.execute();
        }
    }

    public List<ICondition> getConditions() {
        return conditions;
    }

    @Override
    public CharSequence toReadableString(Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(Utils.toReadableString(getConditions(), context));
        stringBuilder.append("\n");
        if (command != null) {
            stringBuilder.append(command.toReadableString(context));
        }
        return stringBuilder;
    }

    @SuppressWarnings("unused")
    public void setConditions(List<ICondition> conditions) {
        this.conditions = conditions;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> hashMap = new HashMap<>();
        hashMap.put(KEY_ID, getIdentifier());
        if (name != null) {
            hashMap.put(KEY_NAME, name);
        }
        List<Map<String, Object>> conditionProperties = new ArrayList<>();
        for (ICondition condition : conditions) {
            conditionProperties.add(condition.toMap());
        }
        hashMap.put(KEY_CONDITIONS, conditionProperties);
        if (command != null) {
            hashMap.put(KEY_COMMAND, command.toMap());
        }
        return hashMap;
    }

    @NonNull
    @Override
    public String getIdentifier() {
        return String.valueOf(identifier);
    }

    public void setIdentifier(long identifier) {
        this.identifier = identifier;
    }

    @Override
    public int hashCode() {
        int result = (int) (identifier ^ (identifier >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + conditions.hashCode();
        result = 31 * result + (command != null ? command.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Configuration that = (Configuration) o;

        if (identifier != that.identifier) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (!conditions.equals(that.conditions)) return false;
        return command != null ? command.equals(that.command) : that.command == null;
    }

    @Override
    public Configuration clone() {
        Configuration configuration = new Configuration();
        configuration.setName(name);
        configuration.setConditions(new ArrayList<>(conditions));
        configuration.setCommand(command);
        return configuration;
    }
}
