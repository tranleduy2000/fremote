package com.duy.iremote.shared.config.command;

import android.content.Context;

import com.duy.iremote.R;
import com.duy.iremote.shared.database.DatabaseManager;
import com.duy.iremote.shared.models.devices.ArduinoPin;
import com.duy.iremote.shared.models.devices.IArduinoPin;
import com.duy.iremote.shared.utils.DLog;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SetPinValueCommand implements ICommand, Cloneable {

    public static final String TYPE = "set_pin_value";
    public static final int LOW = 0;
    public static final int HIGH = 1;
    private static final String TAG = "SetPinValueCommand";
    private int value;
    private int pin;

    public SetPinValueCommand(int pin, int value) {
        this.value = value;
        this.pin = pin;
    }

    public SetPinValueCommand(Map<String, Object> properties) {
        this.value = Integer.parseInt(String.valueOf(properties.get("value")));
        this.pin = Integer.parseInt(String.valueOf(properties.get("pin")));
    }

    @Override
    public void execute() {
        if (DLog.DEBUG) DLog.d(TAG, "execute() called");
        if (DLog.DEBUG) DLog.d(TAG, "pin = " + pin);
        if (DLog.DEBUG) DLog.d(TAG, "value = " + value);


        DatabaseManager databaseManager = DatabaseManager.getInstance();
        if (databaseManager != null) {

            ArrayList<IArduinoPin> devices = databaseManager.getDevices();
            for (IArduinoPin device : devices) {
                if (device.getPin() == pin) {
                    device.setValue(value);
                    databaseManager.updateDevice(device);
                    return;
                }
            }

            // pin is not found, create new pin
            ArduinoPin pin = new ArduinoPin(null, this.pin);
            pin.setValue(value);
            databaseManager.updateDevice(pin);
        }
    }

    @Override
    public CharSequence toReadableString(Context context) {
        return (value == 0 ? context.getString(R.string.turn_off) : context.getString(R.string.turn_on))
                + " " + pin;
    }

    @Override
    public Map<String, Object> toMap() {
        Map<String, Object> map = new HashMap<>();
        map.put(KEY_COMMAND_TYPE, TYPE);
        map.put("value", value);
        map.put("pin", pin);
        return map;
    }

    public int getPin() {
        return pin;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    protected SetPinValueCommand clone() {
        return new SetPinValueCommand(pin, value);
    }

}
