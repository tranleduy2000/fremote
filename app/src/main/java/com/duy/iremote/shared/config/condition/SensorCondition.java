package com.duy.iremote.shared.config.condition;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.duy.iremote.shared.config.ExpressionContext;
import com.duy.iremote.shared.config.ICondition;
import com.duy.iremote.shared.config.input.ConstantValueNode;
import com.duy.iremote.shared.database.DatabaseConstants;

import java.util.HashMap;
import java.util.Map;


public class SensorCondition implements ICondition {
    public static final String SENSOR_CONDITION_TYPE = "sensor_condition";

    private static final String KEY_SENSOR_NAME = "sensor_name";
    private static final String KEY_VALUE = "value";
    private static final String KEY_OPERATOR = "operator";
    @NonNull
    private SensorType sensorType;
    @NonNull
    private RelationOperatorCondition.RelationOperator relationOperator;
    private double targetValue;

    public SensorCondition(@NonNull SensorType sensorType, @NonNull RelationOperatorCondition.RelationOperator relationOperator, double targetValue) {
        this.sensorType = sensorType;
        this.relationOperator = relationOperator;
        this.targetValue = targetValue;
    }

    public SensorCondition(Map<String, Object> conditionProperty) {
        this.sensorType = SensorType.valueOf(String.valueOf(conditionProperty.get(KEY_SENSOR_NAME)));
        this.relationOperator = RelationOperatorCondition.RelationOperator.valueOf(String.valueOf(conditionProperty.get(KEY_OPERATOR)));
        this.targetValue = Double.parseDouble(String.valueOf(conditionProperty.get(KEY_VALUE)));
    }

    @Override
    public boolean test(ExpressionContext context) {
        double sensorValue = context.getVariable(sensorType.getKey());
        return new RelationOperatorCondition(relationOperator, new ConstantValueNode(sensorValue), new ConstantValueNode(targetValue)).test(context);
    }

    @Override
    public CharSequence toReadableString(Context context) {
        return sensorType.getName() + " " + relationOperator.getDisplaySymbol() + " " + new ConstantValueNode(targetValue).toReadableString(context);
    }

    @Override
    public Map<String, Object> toMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put(ICondition.KEY_CONDITION_TYPE, SENSOR_CONDITION_TYPE);
        map.put(KEY_SENSOR_NAME, sensorType.name());
        map.put(KEY_OPERATOR, relationOperator.name());
        map.put(KEY_VALUE, targetValue);
        return map;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getSensorType() != null ? getSensorType().hashCode() : 0;
        result = 31 * result + (getRelationOperator() != null ? getRelationOperator().hashCode() : 0);
        temp = Double.doubleToLongBits(getTargetValue());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SensorCondition that = (SensorCondition) o;

        if (Double.compare(that.getTargetValue(), getTargetValue()) != 0) return false;
        if (getSensorType() != that.getSensorType()) return false;
        return getRelationOperator() == that.getRelationOperator();
    }

    public double getTargetValue() {
        return targetValue;
    }

    public void setTargetValue(double targetValue) {
        this.targetValue = targetValue;
    }

    @NonNull
    public SensorType getSensorType() {
        return sensorType;
    }

    public void setSensorType(@NonNull SensorType sensorType) {
        this.sensorType = sensorType;
    }

    @NonNull
    public RelationOperatorCondition.RelationOperator getRelationOperator() {
        return relationOperator;
    }

    public void setRelationOperator(@NonNull RelationOperatorCondition.RelationOperator relationOperator) {
        this.relationOperator = relationOperator;
    }

    public enum SensorType {
        LIGHT(DatabaseConstants.KEY_LIGHT),
        TEMPERATURE(DatabaseConstants.KEY_TEMPERATURE),
        HUMIDITY(DatabaseConstants.KEY_HUMIDITY);

        private String key;

        SensorType(String key) {
            this.key = key;
        }

        @Nullable
        public static SensorType findSensorTypeWithKey(String key) {
            for (SensorType value : SensorType.values()) {
                if (value.getKey().equalsIgnoreCase(key)) {
                    return value;
                }
            }
            return null;
        }

        public String getKey() {
            return key;
        }

        public String getName() {
            return name();
        }
    }
}
