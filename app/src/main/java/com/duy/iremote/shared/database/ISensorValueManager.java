package com.duy.iremote.shared.database;

import com.duy.iremote.shared.config.condition.SensorCondition;

public interface ISensorValueManager {

    void pushSensorValue(SensorCondition.SensorType sensorType, double value);
}
