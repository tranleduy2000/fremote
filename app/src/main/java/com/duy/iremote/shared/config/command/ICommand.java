package com.duy.iremote.shared.config.command;

import android.content.Context;

import com.duy.iremote.shared.config.FirebaseExportable;

public interface ICommand extends FirebaseExportable {

    String KEY_COMMAND_TYPE = "command_type";

    void execute();

    CharSequence toReadableString(Context context);
}
