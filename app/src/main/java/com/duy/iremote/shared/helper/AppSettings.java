package com.duy.iremote.shared.helper;

import android.content.Context;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.duy.iremote.AppMode;

public class AppSettings {
    private static final String KEY_APP_MODE = "key_app_mode";
    private static final String KEY_LAST_CONNECTED_DEVICE = "key_last_connected_device";

    public static void setMode(Context context, AppMode client) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(KEY_APP_MODE, client.toString()).apply();
    }

    @NonNull
    public static AppMode getMode(Context context) {
        String appMode = PreferenceManager.getDefaultSharedPreferences(context)
                .getString(KEY_APP_MODE, "");
        if (appMode == null || appMode.isEmpty()) {
            return AppMode.CLIENT;
        }
        try {
            return AppMode.valueOf(appMode);
        } catch (Exception e) {
            return AppMode.CLIENT;
        }
    }

    public static void setLastConnectedDevice(Context context, @NonNull String address) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit().putString(KEY_LAST_CONNECTED_DEVICE, address).apply();
    }

    public static String getLastConnectedDevice(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(KEY_LAST_CONNECTED_DEVICE, "");
    }
}
