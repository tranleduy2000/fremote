package com.duy.iremote.shared.database;

import com.duy.iremote.shared.config.IConfiguration;

public interface IConfigurationManager {

    void addConfig(IConfiguration configuration);

    void updateConfig(IConfiguration configuration);

    void removeConfig(IConfiguration configuration);

}
