package com.duy.iremote.shared.database;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.duy.iremote.IRemoteApplication;
import com.duy.iremote.shared.config.Configuration;
import com.duy.iremote.shared.config.IConfiguration;
import com.duy.iremote.shared.config.condition.SensorCondition;
import com.duy.iremote.shared.models.devices.ArduinoPin;
import com.duy.iremote.shared.models.devices.IArduinoPin;
import com.duy.iremote.shared.utils.DLog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Map;

public class DatabaseManager implements IDatabaseManager, OnFailureListener {
    private static final String TAG = "DeviceManager";
    private static DatabaseManager instance;

    private final ArrayList<IArduinoPin> devices = new ArrayList<>();
    private final ArrayList<IConfiguration> configs = new ArrayList<>();

    private DatabaseReference rootDatabase;
    private DatabaseReference devicesDatabase;
    private DatabaseReference configsDatabase;
    private DatabaseReference sensorValueDatabase;

    private SimpleChildEventListener configChildEventListener = new SimpleChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            try {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                configs.add(new Configuration(value));
                sendDatabaseEvent(DatabaseEvents.NEW_CONFIG_ADDED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            try {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                Configuration newValue = new Configuration(value);
                for (int i = 0; i < configs.size(); i++) {
                    IConfiguration config = configs.get(i);
                    if (config.getIdentifier().equals(newValue.getIdentifier())) {
                        configs.set(i, newValue);
                        return;
                    }
                }
                sendDatabaseEvent(DatabaseEvents.CONFIG_CHANGED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            try {
                Map<String, Object> value = (Map<String, Object>) dataSnapshot.getValue();
                Configuration toBeRemoved = new Configuration(value);
                for (int i = 0; i < configs.size(); i++) {
                    IConfiguration config = configs.get(i);
                    if (config.getIdentifier().equals(toBeRemoved.getIdentifier())) {
                        configs.remove(i);
                        sendDatabaseEvent(DatabaseEvents.CONFIG_REMOVED);
                        return;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
    private SimpleChildEventListener deviceChildEventListener = new SimpleChildEventListener() {
        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            try {
                ArduinoPin value = dataSnapshot.getValue(ArduinoPin.class);
                devices.add(value);
                sendDatabaseEvent(DatabaseEvents.NEW_DEVICE_ADDED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
            try {
                ArduinoPin newValue = dataSnapshot.getValue(ArduinoPin.class);
                for (IArduinoPin device : devices) {
                    if (newValue != null && device.getPin() == newValue.getPin()) {
                        device.setValue(newValue.getValue());
                    }
                }
                sendDatabaseEvent(DatabaseEvents.DEVICE_CHANGED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
            try {
                ArduinoPin value = dataSnapshot.getValue(ArduinoPin.class);
                devices.remove(value);
                sendDatabaseEvent(DatabaseEvents.DEVICE_REMOVED);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private DatabaseManager(@NonNull FirebaseUser firebaseUser) {
        rootDatabase = FirebaseDatabase.getInstance()
                .getReference()
                .child(DatabaseConstants.ROOT_DATABASE)
                .child(firebaseUser.getUid());
        devicesDatabase = rootDatabase.child(DatabaseConstants.KEY_DEVICES);
        configsDatabase = rootDatabase.child(DatabaseConstants.KEY_CONFIGS);
        sensorValueDatabase = rootDatabase.child(DatabaseConstants.KEY_SENSOR_VALUES);
        addValueWatcher();
    }

    @Nullable
    public static DatabaseManager getInstance() {
        return instance;
    }

    @NonNull
    public static DatabaseManager getInstance(FirebaseUser firebaseUser) {
        if (instance == null) {
            instance = new DatabaseManager(firebaseUser);
        }
        return instance;
    }

    public static void dispose() {
        DatabaseManager databaseManager = DatabaseManager.instance;
        if (databaseManager != null) {
            databaseManager.devicesDatabase.removeEventListener(databaseManager.deviceChildEventListener);
            databaseManager.configsDatabase.removeEventListener(databaseManager.configChildEventListener);
        }
        DatabaseManager.instance = null;
    }

    private void addValueWatcher() {
        devicesDatabase.addChildEventListener(deviceChildEventListener);
        configsDatabase.addChildEventListener(configChildEventListener);
    }

    private void sendDatabaseEvent(String event) {
        if (DLog.DEBUG) DLog.d(TAG, "sendDatabaseEvent() called with: event = [" + event + "]");
        Intent intent = new Intent();
        intent.setAction(event);
        IRemoteApplication.application.sendBroadcast(intent);
    }

    @NonNull
    public DatabaseReference getRootDatabase() {
        return rootDatabase;
    }

    @NonNull
    public DatabaseReference getDeviceDatabase() {
        return devicesDatabase;
    }

    public DatabaseReference getSensorValueDatabase() {
        return sensorValueDatabase;
    }

    public ArrayList<IArduinoPin> getDevices() {
        return new ArrayList<>(devices);
    }

    public ArrayList<IConfiguration> getConfigs() {
        return new ArrayList<>(configs);
    }

    @Override
    public void addDevice(IArduinoPin device) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "addDevice() called with: digitalDevice = [" + device + "]");
        }
        devicesDatabase.child(device.getIdentifier())
                .setValue(device)
                .addOnFailureListener(this);
    }


    @Override
    public void removeDevice(IArduinoPin device) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "removeDevice() called with: device = [" + device + "]");
        }
        devicesDatabase.child(device.getIdentifier())
                .removeValue()
                .addOnFailureListener(this);
    }

    @Override
    public void updateDevice(IArduinoPin device) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "updateDevice() called with: device = [" + device + "]");
        }
        devicesDatabase.child(device.getIdentifier())
                .setValue(device)
                .addOnFailureListener(this);
    }

    @Override
    public void onFailure(@NonNull Exception e) {
        if (DLog.DEBUG) {
            DLog.e(TAG, "onFailure: ", e);
        }
    }


    @Override
    public void addConfig(IConfiguration configuration) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "addConfig() called with: configuration = [" + configuration + "]");
        }
        configsDatabase.child(configuration.getIdentifier())
                .setValue(configuration.toMap())
                .addOnFailureListener(this);
    }

    @Override
    public void updateConfig(IConfiguration configuration) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "updateConfig() called with: configuration = [" + configuration + "]");
        }
        configsDatabase.child(configuration.getIdentifier())
                .setValue(configuration.toMap())
                .addOnFailureListener(this);
    }

    @Override
    public void removeConfig(IConfiguration configuration) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "removeConfig() called with: configuration = [" + configuration + "]");
        }
        configsDatabase.child(configuration.getIdentifier())
                .removeValue()
                .addOnFailureListener(this);
    }

    @Override
    public void pushSensorValue(SensorCondition.SensorType sensorType, double value) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "pushSensorValue() called with: sensorType = [" + sensorType + "], value = [" + value + "]");
        }
        sensorValueDatabase.child(sensorType.getKey()).setValue(value);
    }
}
