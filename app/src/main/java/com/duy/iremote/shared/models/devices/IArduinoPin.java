package com.duy.iremote.shared.models.devices;

import android.support.annotation.Nullable;

import com.duy.iremote.shared.database.IFirebaseObject;

public interface IArduinoPin extends IFirebaseObject {

    int getPin();

    @Nullable
    String getName();

    int getValue();

    void setValue(int value);
}
