package com.duy.iremote.shared.config;

import android.content.Context;

import java.util.List;

public class Utils {

    public static <T> int findIndex(T[] array, T what) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(what)) {
                return i;
            }
        }
        return -1;
    }

    public static CharSequence toReadableString(List<ICondition> conditions, Context context) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < conditions.size(); i++) {
            ICondition condition = conditions.get(i);
            stringBuilder.append(condition.toReadableString(context));
            if (i != conditions.size() - 1) {
                stringBuilder.append("\n");
            }
        }
        return stringBuilder;
    }
}
