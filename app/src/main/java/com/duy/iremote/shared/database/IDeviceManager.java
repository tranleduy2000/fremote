package com.duy.iremote.shared.database;

import com.duy.iremote.shared.models.devices.IArduinoPin;

public interface IDeviceManager {

    void addDevice(IArduinoPin device);

    void removeDevice(IArduinoPin device);

    void updateDevice(IArduinoPin device);
}
