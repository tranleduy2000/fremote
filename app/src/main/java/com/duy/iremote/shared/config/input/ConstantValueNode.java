package com.duy.iremote.shared.config.input;

import android.content.Context;

import com.duy.iremote.shared.config.ExpressionContext;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.text.DecimalFormat;

public class ConstantValueNode implements Input {
    private static final String VALUE_TAG = "value";
    private double value;

    public ConstantValueNode(double value) {
        this.value = value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public double getValue(ExpressionContext context) {
        return value;
    }

    @Override
    public void writeToXml(Document rootDocument, Element output) {
        output.setAttribute(VALUE_TAG, String.valueOf(value));
    }

    @Override
    public CharSequence toReadableString(Context context) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return decimalFormat.format(value);
    }
}
