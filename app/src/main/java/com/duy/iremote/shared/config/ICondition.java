package com.duy.iremote.shared.config;

import android.content.Context;

public interface ICondition extends FirebaseExportable {

    String KEY_CONDITION_TYPE = "type";

    boolean test(ExpressionContext context);

    CharSequence toReadableString(Context context);
}
