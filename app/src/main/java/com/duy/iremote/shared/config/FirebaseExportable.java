package com.duy.iremote.shared.config;

import java.util.Map;

public interface FirebaseExportable {
    Map<String, Object> toMap();
}
