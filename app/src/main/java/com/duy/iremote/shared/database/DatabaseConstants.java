package com.duy.iremote.shared.database;

import com.duy.iremote.R;

public class DatabaseConstants {
    public static final String ROOT_DATABASE = "iremote";

    public static final String KEY_HUMIDITY = "humidity";
    public static final String KEY_TEMPERATURE = "temperature";
    public static final String KEY_LIGHT = "light";

    public static final String KEY_DEVICES = "devices";
    public static final String KEY_CONFIGS = "configs";
    public static final String KEY_SENSOR_VALUES = "sensor_values";

    public static final Integer[] DEVICE_ICON_IDS = new Integer[]{
            R.drawable.ic_device_fan_128,
            R.drawable.ic_device_light_128,
    };

}
