package com.duy.iremote.shared.models.devices;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class ArduinoPin implements IArduinoPin {

    public static final int HIGH = 1;
    public static final int LOW = 0;

    @Nullable
    protected String name;
    protected int value;
    private int pin;

    @SuppressWarnings("unused")
    ArduinoPin() {

    }

    public ArduinoPin(@Nullable String name, int pin) {
        this.name = name;
        this.pin = pin;
    }

    @NonNull
    @Override
    public String getIdentifier() {
        return String.valueOf(pin);
    }

    @Override
    public int hashCode() {
        int result = getPin();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + getValue();
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ArduinoPin)) return false;

        ArduinoPin that = (ArduinoPin) o;

        if (getPin() != that.getPin()) return false;
        if (getValue() != that.getValue()) return false;
        return getName() != null ? getName().equals(that.getName()) : that.getName() == null;
    }

    @NonNull
    @Override
    public String toString() {
        return "DigitalPin{" +
                "name='" + name + '\'' +
                ", value=" + value +
                ", pin=" + pin +
                '}';
    }

    @Override
    public int getPin() {
        return pin;
    }

    @Nullable
    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getValue() {
        return value;
    }

    @Override
    public void setValue(int value) {
        this.value = value;
    }

    public void setName(@Nullable String name) {
        this.name = name;
    }

    public void setPin(int pin) {
        this.pin = pin;
    }
}
