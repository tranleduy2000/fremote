package com.duy.iremote.client.dashboard.devices;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.duy.iremote.R;
import com.duy.iremote.shared.database.IDatabaseManager;
import com.duy.iremote.shared.models.devices.IArduinoPin;
import com.github.angads25.toggle.LabeledSwitch;
import com.github.angads25.toggle.interfaces.OnToggledListener;

import java.util.ArrayList;
import java.util.List;

public class DevicesAdapter extends RecyclerView.Adapter<DevicesAdapter.ViewHolder> {

    private final LayoutInflater layoutInflater;
    private final IDatabaseManager deviceManager;
    private List<IArduinoPin> devices = new ArrayList<>();

    DevicesAdapter(Context context, IDatabaseManager deviceManager) {
        this.deviceManager = deviceManager;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public DevicesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.list_item_deivce_digital, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull final DevicesAdapter.ViewHolder holder, int position) {
        final IArduinoPin device = devices.get(position);
        holder.txtName.setText(device.getName());
        holder.txtDescription.setText("Pin " + String.valueOf(device.getPin()));

        holder.statusView.setOn(device.getValue() != 0);
        final OnToggledListener onToggledListener = new OnToggledListener() {
            @Override
            public void onSwitched(LabeledSwitch labeledSwitch, boolean isOn) {
                device.setValue(isOn ? 1 : 0);
                deviceManager.updateDevice(device);
            }
        };
        holder.statusView.setOnToggledListener(onToggledListener);
        holder.openMenuView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayMenuFor(v, device);
            }
        });
        holder.rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.statusView.setOn(!holder.statusView.isOn());
                onToggledListener.onSwitched(holder.statusView, holder.statusView.isOn());
            }
        });
    }

    @Override
    public int getItemCount() {
        return devices.size();
    }

    private void displayMenuFor(View view, final IArduinoPin device) {
        PopupMenu popupMenu = new PopupMenu(view.getContext(), view);
        popupMenu.getMenuInflater().inflate(R.menu.menu_device_item, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.action_edit_device:
                        break;
                    case R.id.action_remove_device:
                        deviceManager.removeDevice(device);
                        int index = devices.indexOf(device);
                        devices.remove(index);
                        notifyItemRemoved(index);
                        return true;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    public void setDevices(List<IArduinoPin> devices) {
        this.devices = devices;
        notifyDataSetChanged();
    }

    public void add(IArduinoPin digitalDevice) {
        devices.add(digitalDevice);
        notifyItemInserted(devices.size() - 1);
    }

    void clearAll() {
        devices.clear();
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtName;
        private TextView txtDescription;
        private View openMenuView;
        private LabeledSwitch statusView;
        private View rootView;
        private ImageView iconView;

        ViewHolder(View itemView) {
            super(itemView);
            txtName = itemView.findViewById(R.id.txt_device_name);
            txtDescription = itemView.findViewById(R.id.txt_description);
            openMenuView = itemView.findViewById(R.id.btn_more_action);
            statusView = itemView.findViewById(R.id.status_view);
            rootView = itemView.findViewById(R.id.root_view);
            iconView = itemView.findViewById(R.id.img_icon);
        }
    }
}
