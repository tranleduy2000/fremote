package com.duy.iremote.client.dashboard.config;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.duy.iremote.R;
import com.duy.iremote.shared.config.ICondition;
import com.duy.iremote.shared.config.Utils;
import com.duy.iremote.shared.config.condition.RelationOperatorCondition;
import com.duy.iremote.shared.config.condition.SensorCondition;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ConditionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int BOOLEAN_EXPRESSION = 1;

    private final List<ICondition> conditions;
    private final LayoutInflater layoutInflater;

    ConditionAdapter(Context context, List<ICondition> conditions) {
        this.conditions = conditions;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        switch (viewType) {
            case BOOLEAN_EXPRESSION:
                View view = layoutInflater.inflate(R.layout.list_item_sensor_condition_view, viewGroup, false);
                return new SensorConditionViewHolder(view);
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        ICondition condition = conditions.get(position);
        if (viewHolder instanceof SensorConditionViewHolder) {
            ((SensorConditionViewHolder) viewHolder).setCondition((SensorCondition) condition);
            ((SensorConditionViewHolder) viewHolder).btnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int adapterPosition = viewHolder.getAdapterPosition();
                    conditions.remove(adapterPosition);
                    notifyItemRemoved(adapterPosition);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        ICondition condition = conditions.get(position);
        if (condition instanceof SensorCondition) {
            return BOOLEAN_EXPRESSION;
        }
        return super.getItemViewType(position);
    }

    @Override
    public int getItemCount() {
        return conditions.size();
    }

    private static class SensorConditionViewHolder extends RecyclerView.ViewHolder {

        private Spinner listSensorView;
        private Spinner listOperatorView;
        private EditText editValue;
        private View btnRemove;

        SensorConditionViewHolder(@NonNull View itemView) {
            super(itemView);
            this.listSensorView = itemView.findViewById(R.id.list_sensor_view);
            this.listOperatorView = itemView.findViewById(R.id.list_operator_view);
            this.editValue = itemView.findViewById(R.id.edit_value);
            this.btnRemove = itemView.findViewById(R.id.btn_remove);
        }

        void setCondition(SensorCondition condition) {

            listSensorView.setAdapter(new ArrayAdapter<>(itemView.getContext(),
                    android.R.layout.simple_list_item_1,
                    Arrays.stream(SensorCondition.SensorType.values()).map(sensor -> sensor.getName()).collect(Collectors.toList())));
            listSensorView.setSelection(Utils.findIndex(SensorCondition.SensorType.values(), condition.getSensorType()));
            listSensorView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    condition.setSensorType(SensorCondition.SensorType.values()[position]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            listOperatorView.setAdapter(new ArrayAdapter<>(itemView.getContext(),
                    android.R.layout.simple_list_item_1,
                    Arrays.stream(RelationOperatorCondition.RelationOperator.values()).map(op -> op.getDisplaySymbol()).collect(Collectors.toList())));
            listOperatorView.setSelection(Utils.findIndex(RelationOperatorCondition.RelationOperator.values(), condition.getRelationOperator()));
            listOperatorView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    condition.setRelationOperator(RelationOperatorCondition.RelationOperator.values()[position]);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

            editValue.setText(String.valueOf(condition.getTargetValue()));
            editValue.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    try {
                        condition.setTargetValue(Double.parseDouble(s.toString()));
                    } catch (Exception e) {
                        editValue.setError(e.getMessage());
                    }
                }
            });
        }
    }
}
