package com.duy.iremote.client.dashboard.config;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.duy.iremote.R;
import com.duy.iremote.shared.config.Configuration;
import com.duy.iremote.shared.config.IConfiguration;
import com.duy.iremote.shared.database.DatabaseEvents;
import com.duy.iremote.shared.database.DatabaseManager;
import com.duy.iremote.shared.database.IConfigurationManager;
import com.duy.iremote.shared.models.ResultCallback;
import com.duy.iremote.shared.utils.DLog;
import com.google.firebase.auth.FirebaseAuth;

public class ConfigurationFragment extends Fragment {

    private static final String TAG = "ConfigurationFragment";

    private ConfigurationAdapter adapter;
    private IConfigurationManager configurationManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BroadcastReceiver updateConfigListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DLog.DEBUG) {
                DLog.d(TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");
            }
            reloadData();
        }
    };

    public static ConfigurationFragment newInstance() {

        Bundle args = new Bundle();

        ConfigurationFragment fragment = new ConfigurationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        this.configurationManager = DatabaseManager.getInstance(FirebaseAuth.getInstance().getCurrentUser());

        getContext().registerReceiver(updateConfigListener, new IntentFilter(DatabaseEvents.NEW_CONFIG_ADDED));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_configuration, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(false);
        adapter = new ConfigurationAdapter(getContext(), configurationManager);
        recyclerView.setAdapter(adapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadData();
            }
        });
        reloadData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(updateConfigListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_config_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_config:
                showDialogAddNewConfig();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void reloadData() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }
        //hide progress
        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 300);

        adapter.setConfigurations(DatabaseManager.getInstance(FirebaseAuth.getInstance().getCurrentUser()).getConfigs());
    }

    private void showDialogEditConfig(Configuration configuration) {
        Configuration temporary = configuration.clone();
    }

    private void showDialogAddNewConfig() {
        Configuration configuration = new Configuration();
        configuration.setName(getContext().getString(R.string.no_name));
        ConfigurationEditorDialog.newInstance(configuration, new ResultCallback<IConfiguration>() {
            @Override
            public void onSuccess(IConfiguration result) {
                configurationManager.addConfig(result);
            }

            @Override
            public void onFailure(@Nullable Exception e) {

            }
        }).show(getChildFragmentManager(), ConfigurationEditorDialog.class.getSimpleName());
    }

}
