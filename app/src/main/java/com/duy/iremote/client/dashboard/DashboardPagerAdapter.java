package com.duy.iremote.client.dashboard;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.duy.iremote.client.dashboard.config.ConfigurationFragment;
import com.duy.iremote.client.dashboard.devices.DevicesFragment;
import com.duy.iremote.client.dashboard.sensors.SensorValueFragment;

public class DashboardPagerAdapter extends FragmentPagerAdapter {
    DashboardPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return DevicesFragment.newInstance();
            case 1:
                return ConfigurationFragment.newInstance();
            case 2:
                return SensorValueFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
