package com.duy.iremote.client.dashboard.devices;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.duy.iremote.R;
import com.duy.iremote.shared.database.DatabaseConstants;
import com.duy.iremote.shared.models.ResultCallback;
import com.duy.iremote.shared.models.devices.ArduinoPin;
import com.duy.iremote.shared.models.devices.IArduinoPin;


public class AddingDeviceDialog extends AppCompatDialog {
    private EditText editPin;
    private EditText editDeviceName;
    private Spinner iconList;

    private ResultCallback<IArduinoPin> onCompleteListener;

    AddingDeviceDialog(Context context, ResultCallback<IArduinoPin> onCompleteListener) {
        super(context);
        this.onCompleteListener = onCompleteListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_new_device);
        editDeviceName = findViewById(R.id.edit_device_name);
        editPin = findViewById(R.id.edit_device_pin);
        iconList = findViewById(R.id.spinner_icons);
        iconList.setAdapter(new DeviceIconAdapter(getContext(), -1, DatabaseConstants.DEVICE_ICON_IDS));

        findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        findViewById(R.id.btn_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidData()) {
                    try {
                        int pin = Integer.parseInt(editPin.getText().toString());
                        String name = editDeviceName.getText().toString();
                        ArduinoPin digitalDevice = new ArduinoPin(name, pin);
                        if (onCompleteListener != null) {
                            onCompleteListener.onSuccess(digitalDevice);
                        }
                        dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        setCancelable(false);
        setCanceledOnTouchOutside(false);
    }

    private boolean isValidData() {
        if (editPin.getText().toString().trim().isEmpty()) {
            editPin.setError(getContext().getString(R.string.enter_device_pin));
            return false;
        }
        return true;
    }

    public interface OnCompleteListener {
        void onComplete(IArduinoPin device);
    }
}
