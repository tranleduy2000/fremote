package com.duy.iremote.client.dashboard.devices;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.duy.iremote.R;
import com.duy.iremote.shared.database.DatabaseEvents;
import com.duy.iremote.shared.database.DatabaseManager;
import com.duy.iremote.shared.database.IDatabaseManager;
import com.duy.iremote.shared.models.ResultCallback;
import com.duy.iremote.shared.models.devices.IArduinoPin;
import com.duy.iremote.shared.utils.DLog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DevicesFragment extends Fragment {

    private static final String TAG = "DevicesFragment";

    private DevicesAdapter devicesAdapter;
    private IDatabaseManager databaseManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private BroadcastReceiver updateDevicesListener = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (DLog.DEBUG) {
                DLog.d(TAG, "onReceive() called with: context = [" + context + "], intent = [" + intent + "]");
            }
            reloadData();
        }
    };

    public static DevicesFragment newInstance() {

        Bundle args = new Bundle();

        DevicesFragment fragment = new DevicesFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        databaseManager = DatabaseManager.getInstance(currentUser);
        getContext().registerReceiver(updateDevicesListener, new IntentFilter(DatabaseEvents.NEW_DEVICE_ADDED));
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_devices, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView deviceListView = view.findViewById(R.id.device_list_view);
        deviceListView.setHasFixedSize(false);
        deviceListView.setLayoutManager(new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL));
        devicesAdapter = new DevicesAdapter(getContext(), databaseManager);
        deviceListView.setAdapter(devicesAdapter);

        swipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                reloadData();
            }
        });

        reloadData();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        getContext().unregisterReceiver(updateDevicesListener);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_devices_fragment, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_device:
                showDialogAddDevice();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialogAddDevice() {
        AddingDeviceDialog addingDeviceDialog = new AddingDeviceDialog(getContext(),
                new ResultCallback<IArduinoPin>() {
                    @Override
                    public void onSuccess(IArduinoPin result) {
                        databaseManager.addDevice(result);
                        reloadData();
                    }

                    @Override
                    public void onFailure(@Nullable Exception e) {

                    }
                });
        addingDeviceDialog.show();
    }

    private void reloadData() {
        if (!swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(true);
        }
        //hide progress
        swipeRefreshLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, 300);

        devicesAdapter.setDevices(DatabaseManager.getInstance(FirebaseAuth.getInstance().getCurrentUser()).getDevices());

    }

}
