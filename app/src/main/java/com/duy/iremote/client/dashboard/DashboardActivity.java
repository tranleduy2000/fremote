package com.duy.iremote.client.dashboard;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.duy.iremote.AppMode;
import com.duy.iremote.R;
import com.duy.iremote.server.ServerActivity;
import com.duy.iremote.server.services.FRemoteService;
import com.duy.iremote.shared.database.DatabaseManager;
import com.duy.iremote.shared.helper.AppSettings;
import com.duy.iremote.shared.login.LoginActivity;
import com.duy.iremote.shared.views.CustomViewPager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private FirebaseUser firebaseUser;
    private Toolbar toolbar;
    private CustomViewPager viewPager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle(R.string.title_activity_dashboard);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        initNavigationView();

        viewPager = findViewById(R.id.view_pager);
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(new DashboardPagerAdapter(getSupportFragmentManager()));
        viewPager.setOffscreenPageLimit(viewPager.getAdapter().getCount());

        initBottomNavigationView();
    }

    private void initNavigationView() {
        NavigationView navigationView = findViewById(R.id.navigation_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView txtName = headerView.findViewById(R.id.txt_name);
        String displayName = firebaseUser.getDisplayName();
        displayName += "\n" + firebaseUser.getEmail();
        txtName.setText(displayName);

        DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.open_drawer, R.string.close_drawer);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

    }

    private void initBottomNavigationView() {
        final BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_nav_view);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {

                    case R.id.action_show_devices:
                        viewPager.setCurrentItem(0);
                        return true;

                    case R.id.action_show_conditions:
                        viewPager.setCurrentItem(1);
                        return true;

                    case R.id.action_show_sensor_values:
                        viewPager.setCurrentItem(2);
                        return true;

                }
                return false;
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        bottomNavigationView.setSelectedItemId(R.id.action_show_devices);
                        break;
                    case 1:
                        bottomNavigationView.setSelectedItemId(R.id.action_show_conditions);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sign_out:
                signOut();
                return true;
            case R.id.action_switch_server_mode:
                AppSettings.setMode(this, AppMode.SEVER);
                startActivity(new Intent(this, ServerActivity.class));
                finish();
                break;
        }
        return false;
    }

    private void signOut() {
        stopService(new Intent(this, FRemoteService.class));

        DatabaseManager.dispose();
        FirebaseAuth.getInstance().signOut();

        GoogleSignInOptions gos = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignInClient googleSignInClient = GoogleSignIn.getClient(this, gos);
        googleSignInClient.signOut().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                overridePendingTransition(0, 0);
                startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                finish();
            }
        });

    }
}
