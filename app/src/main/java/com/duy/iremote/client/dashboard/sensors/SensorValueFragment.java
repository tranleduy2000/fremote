package com.duy.iremote.client.dashboard.sensors;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duy.iremote.R;
import com.duy.iremote.shared.database.DatabaseConstants;
import com.duy.iremote.shared.database.DatabaseManager;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class SensorValueFragment extends Fragment {
    public static final String PACKAGE_NAME = "com.duy.iremote.client.dashboard.sensors";

    private TextView txtTemperature;
    private TextView txtHumidity;

    private DatabaseReference sensorDatabase;

    private ValueEventListener temperatureValueWatcher;
    private ValueEventListener humidityValueWatcher;

    public static SensorValueFragment newInstance() {
        Bundle args = new Bundle();
        SensorValueFragment fragment = new SensorValueFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        sensorDatabase = DatabaseManager.getInstance(firebaseUser).getSensorValueDatabase();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sensor, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtHumidity = view.findViewById(R.id.txt_humidity);
        txtTemperature = view.findViewById(R.id.txt_temperature);

        fetchTemperatureAndHumidity();
    }


    private void fetchTemperatureAndHumidity() {
        temperatureValueWatcher = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (txtTemperature != null) {
                    try {
                        Float value = dataSnapshot.getValue(Float.class);
                        if (value == null) {
                            txtTemperature.setText(R.string.not_available);
                        } else {
                            txtTemperature.setText(String.valueOf(value));
                            txtTemperature.append("°C");
                        }
                    } catch (Exception e) {
                        txtTemperature.setText(R.string.not_available);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        humidityValueWatcher = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (txtHumidity != null) {
                    try {
                        Float value = dataSnapshot.getValue(Float.class);
                        if (value == null) {
                            txtHumidity.setText(R.string.not_available);
                        } else {
                            txtHumidity.setText(String.valueOf(value));
                            txtHumidity.append("%");
                        }
                    } catch (Exception e) {
                        txtHumidity.setText(R.string.not_available);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        };
        sensorDatabase.child(DatabaseConstants.KEY_HUMIDITY).addValueEventListener(humidityValueWatcher);
        sensorDatabase.child(DatabaseConstants.KEY_TEMPERATURE).addValueEventListener(temperatureValueWatcher);
    }

    @Override
    public void onDestroyView() {
        sensorDatabase.removeEventListener(humidityValueWatcher);
        sensorDatabase.removeEventListener(temperatureValueWatcher);
        super.onDestroyView();

    }

}
