package com.duy.iremote.client.dashboard.config;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.duy.iremote.R;
import com.duy.iremote.server.command.CommandConstants;
import com.duy.iremote.shared.config.Configuration;
import com.duy.iremote.shared.config.ICondition;
import com.duy.iremote.shared.config.IConfiguration;
import com.duy.iremote.shared.config.command.SetPinValueCommand;
import com.duy.iremote.shared.config.condition.RelationOperatorCondition;
import com.duy.iremote.shared.config.condition.SensorCondition;
import com.duy.iremote.shared.models.ResultCallback;

import java.util.ArrayList;
import java.util.List;

public class ConfigurationEditorDialog extends AppCompatDialogFragment {

    private Configuration configuration;
    private ConditionAdapter conditionAdapter;
    private ResultCallback<IConfiguration> resultCallback;

    private Spinner actionListView;
    private EditText editPinNumber;

    public static ConfigurationEditorDialog newInstance(@Nullable Configuration configuration,
                                                        ResultCallback<IConfiguration> resultCallback) {

        Bundle args = new Bundle();

        ConfigurationEditorDialog fragment = new ConfigurationEditorDialog();
        fragment.setArguments(args);
        fragment.setConfiguration(configuration);
        fragment.setResultCallback(resultCallback);
        return fragment;
    }

    private void setResultCallback(ResultCallback<IConfiguration> resultCallback) {
        this.resultCallback = resultCallback;
    }


    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_edit_configuration, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (configuration == null) {
            dismiss();
            return;
        }

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        toolbar.setTitle(configuration.getName());
        toolbar.setNavigationIcon(R.drawable.round_close_24);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        actionListView = view.findViewById(R.id.action_list_view);
        editPinNumber = view.findViewById(R.id.edit_device_pin);

        // set default value
        if (configuration.getCommand() == null) {
            configuration.setCommand(new SetPinValueCommand(39, SetPinValueCommand.HIGH));
        }

        editPinNumber.setText(String.valueOf(configuration.getCommand().getPin()));
        editPinNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                try {
                    configuration.getCommand().setPin(Integer.parseInt(String.valueOf(s)));
                } catch (Exception e) {
                    e.printStackTrace();
                    editPinNumber.setError(e.getMessage());
                }
            }
        });

        actionListView.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0: { // on
                        SetPinValueCommand oldCommand = configuration.getCommand();
                        configuration.setCommand(new SetPinValueCommand(oldCommand.getPin(), Integer.parseInt(CommandConstants.HIGH)));
                        break;
                    }
                    case 1: { // off
                        SetPinValueCommand oldCommand = configuration.getCommand();
                        configuration.setCommand(new SetPinValueCommand(oldCommand.getPin(), Integer.parseInt(CommandConstants.LOW)));
                        break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        LinearLayoutManager layout = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layout);
        recyclerView.setHasFixedSize(false);
        conditionAdapter = new ConditionAdapter(getContext(), configuration.getConditions());
        recyclerView.setAdapter(conditionAdapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        EditText editName = view.findViewById(R.id.edit_name);
        editName.setText(configuration.getName());
        editName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                configuration.setName(s.toString());
            }
        });

        view.findViewById(R.id.btn_add_condition).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogAddCondition();
            }
        });

        view.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickSave();
            }
        });
    }

    private void clickSave() {
        if (editPinNumber.length() == 0) {
            editPinNumber.setError(getString(R.string.device_pin_empty));
            return;
        }
        if (configuration.getConditions().isEmpty()) {
            Toast.makeText(getContext(), getString(R.string.condition_is_empty), Toast.LENGTH_SHORT).show();
            return;
        }
        if (resultCallback != null) {
            resultCallback.onSuccess(configuration);
        }
        dismiss();
    }

    private void showDialogAddCondition() {
        List<ICondition> conditions = new ArrayList<>();
        conditions.add(new SensorCondition(SensorCondition.SensorType.LIGHT, RelationOperatorCondition.RelationOperator.GREATER, 0));
        conditions.add(new SensorCondition(SensorCondition.SensorType.TEMPERATURE, RelationOperatorCondition.RelationOperator.GREATER, 0));
        conditions.add(new SensorCondition(SensorCondition.SensorType.HUMIDITY, RelationOperatorCondition.RelationOperator.GREATER, 0));

        String[] names = conditions.stream().map(c -> c.toReadableString(getContext())).toArray(String[]::new);
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Select condition type");
        builder.setSingleChoiceItems(names, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (configuration != null) {
                    configuration.addCondition(conditions.get(which));
                    conditionAdapter.notifyDataSetChanged();
                }
                dialog.dismiss();
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
    }


}

