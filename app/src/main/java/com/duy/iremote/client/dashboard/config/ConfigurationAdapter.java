package com.duy.iremote.client.dashboard.config;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duy.iremote.R;
import com.duy.iremote.shared.config.IConfiguration;
import com.duy.iremote.shared.database.IConfigurationManager;

import java.util.ArrayList;
import java.util.List;

class ConfigurationAdapter extends RecyclerView.Adapter<ConfigurationAdapter.ViewHolder> {
    @NonNull
    private List<IConfiguration> configurations = new ArrayList<>();
    private LayoutInflater layoutInflater;
    private IConfigurationManager configurationManager;

    ConfigurationAdapter(Context context, IConfigurationManager configurationManager) {
        this.layoutInflater = LayoutInflater.from(context);
        this.configurationManager = configurationManager;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = layoutInflater.inflate(R.layout.list_item_configuration, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        IConfiguration configuration = configurations.get(i);
        viewHolder.txtTitle.setText(configuration.getName());
        viewHolder.txtDescription.setText(configuration.toReadableString(viewHolder.itemView.getContext()));
        viewHolder.btnRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                configurations.remove(configuration);
                notifyItemRemoved(viewHolder.getAdapterPosition());
                configurationManager.removeConfig(configuration);
            }
        });
    }

    @Override
    public int getItemCount() {
        return configurations.size();
    }

    void setConfigurations(@NonNull List<IConfiguration> configurations) {
        this.configurations = configurations;
        notifyDataSetChanged();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTitle;
        private TextView txtDescription;
        private View btnEdit, btnRemove;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txtTitle = itemView.findViewById(R.id.txt_title);
            this.txtDescription = itemView.findViewById(R.id.txt_description);
            this.btnEdit = itemView.findViewById(R.id.btn_edit);
            this.btnRemove = itemView.findViewById(R.id.btn_remove);
        }
    }
}
