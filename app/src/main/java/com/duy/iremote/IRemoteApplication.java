package com.duy.iremote;

import android.app.Application;
import android.preference.PreferenceManager;

public class IRemoteApplication extends Application {

    public static Application application;

    @Override
    public void onCreate() {
        super.onCreate();
        PreferenceManager.setDefaultValues(this, R.xml.settings, false);
        application = this;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        application = null;
    }
}
