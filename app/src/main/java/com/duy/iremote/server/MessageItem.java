package com.duy.iremote.server;

import android.support.annotation.NonNull;

public class MessageItem {
    public static final int TYPE_INPUT_FROM_ADRUINO = 0;
    public static final int TYPE_OUT = 1;
    public static final int TYPE_ERROR = 2;

    private final int type;
    private final String content;

    public MessageItem(int type, String content) {
        this.type = type;
        this.content = content;
    }

    public int getType() {
        return type;
    }

    public String getContent() {
        return content;
    }

    @NonNull
    @Override
    public String toString() {
        return "MessageItem{" +
                "type=" + type +
                ", content='" + content + '\'' +
                '}';
    }

}
