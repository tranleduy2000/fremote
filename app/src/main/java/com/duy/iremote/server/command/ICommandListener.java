package com.duy.iremote.server.command;

import android.support.annotation.NonNull;

import com.duy.iremote.shared.config.condition.SensorCondition;

public interface ICommandListener {
    void onSensorValueChanged(@NonNull SensorCondition.SensorType sensorType, double value);
}
