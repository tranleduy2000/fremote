package com.duy.iremote.server.services;

import com.duy.iremote.server.command.CommandConstants;
import com.duy.iremote.server.command.ICommandListener;
import com.duy.iremote.server.command.ICommandProcessor;
import com.duy.iremote.shared.config.condition.SensorCondition;

public class ArduinoCommandProcessor implements ICommandProcessor {

    @Override
    public void process(String command, ICommandListener commandListener) {
        if (command == null || command.isEmpty()) {
            return;
        }
        String[] data = command.split("\\s+");
        if (data.length >= 3) {
            switch (data[0]) {
                case CommandConstants.VALUE:
                    String deviceName = data[1];
                    SensorCondition.SensorType sensorType = SensorCondition.SensorType.findSensorTypeWithKey(deviceName);
                    if (sensorType != null) {
                        try {
                            double value = Double.parseDouble(data[2]);
                            if (commandListener != null) {
                                commandListener.onSensorValueChanged(sensorType, value);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        if (deviceName.matches("[0-9]+")) {
                            try {
                                double pin = Integer.parseInt(data[1]);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
            }
        }
    }
}
