package com.duy.iremote.server.services;

public interface IConnectListener {
    void onConnectStatusChanged(boolean isConnected);
}
