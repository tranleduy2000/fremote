package com.duy.iremote.server;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duy.iremote.R;

import java.util.ArrayList;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageHolder> {
    private final ArrayList<MessageItem> messageItems = new ArrayList<>();
    private LayoutInflater layoutInflater;

    public MessageAdapter(Context context) {
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MessageHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case MessageItem.TYPE_INPUT_FROM_ADRUINO:
                return new MessageHolder(
                        layoutInflater.inflate(R.layout.list_item_message_in, parent, false));
            case MessageItem.TYPE_ERROR:
                return new MessageHolder(
                        layoutInflater.inflate(R.layout.list_item_message_error, parent, false));
            default:
                return new MessageHolder(
                        layoutInflater.inflate(R.layout.list_item_message_out, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull MessageHolder holder, int position) {
        holder.txtContent.setText(messageItems.get(position).getContent());
    }

    @Override
    public int getItemViewType(int position) {
        return messageItems.get(position).getType();
    }

    @Override
    public int getItemCount() {
        return messageItems.size();
    }

    public void add(MessageItem message) {
        messageItems.add(message);
        notifyItemInserted(messageItems.size() - 1);
    }


    public void clear() {
        messageItems.clear();
        notifyDataSetChanged();
    }

    class MessageHolder extends RecyclerView.ViewHolder {

        private TextView txtContent;

        MessageHolder(View itemView) {
            super(itemView);
            txtContent = itemView.findViewById(R.id.txt_content);
        }
    }

}
