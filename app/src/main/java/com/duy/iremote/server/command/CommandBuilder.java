package com.duy.iremote.server.command;

import java.util.ArrayList;
import java.util.Iterator;

public class CommandBuilder {

    public static final String CMD_DELIMITER = ";";
    public static final String ARG_DELIMITER = " ";
    private ArrayList<String> args = new ArrayList<>();

    public CommandBuilder() {
    }

    public CommandBuilder set() {
        add(CommandConstants.SET);
        return this;
    }

    public CommandBuilder on() {
        add(CommandConstants.LOW);
        return this;
    }

    public CommandBuilder add(String arg) {
        args.add(arg);
        return this;
    }

    public CommandBuilder off() {
        add(CommandConstants.HIGH);
        return this;
    }

    public CommandBuilder pin(int pin) {
        return add(String.valueOf(pin));
    }

    public String build() {
        StringBuilder sb = new StringBuilder();
        Iterator<?> it = args.iterator();
        if (it.hasNext()) {
            sb.append(it.next());
            while (it.hasNext()) {
                sb.append(ARG_DELIMITER);
                sb.append(it.next());
            }
        }
        sb.append(CMD_DELIMITER);
        return sb.toString();
    }

}
