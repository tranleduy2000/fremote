package com.duy.iremote.server.services;

import android.bluetooth.BluetoothSocket;
import android.support.annotation.NonNull;

import com.duy.iremote.server.MessageItem;
import com.duy.iremote.shared.utils.DLog;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

class BluetoothCommunicator extends Thread {
    private static final String TAG = "ReadThread";
    private BluetoothSocket socket;
    private IMessageListener listener;

    private BufferedReader reader;
    private BufferedWriter writer;

    BluetoothCommunicator(@NonNull BluetoothSocket socket, IMessageListener listener) {
        this.socket = socket;
        this.listener = listener;
        try {
            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        super.run();
        try {
            while (!isInterrupted()) {
                String line = reader.readLine();
                if (listener != null) {
                    listener.onNewMessage(new MessageItem(MessageItem.TYPE_INPUT_FROM_ADRUINO, line));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void write(String message) throws IOException {
        if (DLog.DEBUG) {
            DLog.d(TAG, "write() called with: message = [" + message + "]");
        }
        writer.write(message);
        writer.flush();
    }

    void disconnect() throws Exception {
        interrupt();

        writer.flush();
        writer.close();

        reader.close();

        socket.close();
    }

    boolean isConnected() {
        try {
            return socket.isConnected();
        } catch (Exception e) {
            return false;
        }
    }
}
