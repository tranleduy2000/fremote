package com.duy.iremote.server.services;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.os.AsyncTask;

import com.duy.iremote.shared.models.ResultCallback;

import java.util.UUID;

class ConnectBluetoothTask extends AsyncTask<Void, Void, BluetoothSocket> {

    private BluetoothDevice bluetoothDevice;
    private ResultCallback<BluetoothSocket> callback;
    private Exception exception;

    ConnectBluetoothTask(BluetoothDevice bluetoothDevice, ResultCallback<BluetoothSocket> resultCallback) {
        this.bluetoothDevice = bluetoothDevice;
        this.callback = resultCallback;
    }

    @Override
    protected BluetoothSocket doInBackground(Void... voids) {
        try {
            // SPP UUID service
            final UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
            BluetoothSocket socket = bluetoothDevice.createRfcommSocketToServiceRecord(uuid);
            BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
            socket.connect();
            return socket;
        } catch (Exception e) {
            e.printStackTrace();
            this.exception = e;
        }
        return null;
    }

    @Override
    protected void onPostExecute(BluetoothSocket bluetoothSocket) {
        super.onPostExecute(bluetoothSocket);
        if (isCancelled()) {
            return;
        }
        if (bluetoothSocket != null) {
            callback.onSuccess(bluetoothSocket);
        } else {
            callback.onFailure(exception);
        }
    }
}
