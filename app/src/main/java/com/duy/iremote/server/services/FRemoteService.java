package com.duy.iremote.server.services;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;

import com.duy.iremote.server.MessageItem;
import com.duy.iremote.server.command.CommandBuilder;
import com.duy.iremote.server.command.ICommandListener;
import com.duy.iremote.server.command.ICommandProcessor;
import com.duy.iremote.shared.config.IConfiguration;
import com.duy.iremote.shared.config.condition.SensorCondition;
import com.duy.iremote.shared.database.DatabaseManager;
import com.duy.iremote.shared.database.SimpleChildEventListener;
import com.duy.iremote.shared.helper.AppSettings;
import com.duy.iremote.shared.models.ResultCallback;
import com.duy.iremote.shared.models.devices.ArduinoPin;
import com.duy.iremote.shared.utils.DLog;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Queue;
import java.util.Set;

public class FRemoteService extends Service implements IBluetoothService, IMessageListener, ICommandListener {
    private static final String TAG = "FRemoteService";

    private IBinder binder;
    @Nullable
    private IConnectListener connectionListener;
    private ArrayList<IMessageListener> messageListeners;
    @Nullable
    private WeakReference<ConnectBluetoothTask> connectBluetoothTask;
    @Nullable
    private BluetoothCommunicator bluetoothCommunicator;

    private DatabaseReference deviceDatabase;
    private ChildEventListener deviceStatusListener;
    private DatabaseManager databaseManager;
    private ICommandProcessor commandProcessor = new ArduinoCommandProcessor();

    private HashMap<String, Double> sensorValues = new HashMap<>();
    private Queue<String> commandQueue = new ArrayDeque<>();

    @Override
    public void onCreate() {
        super.onCreate();
        binder = new FRemoteServiceBinder();
        messageListeners = new ArrayList<>();

        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
        this.deviceDatabase = DatabaseManager.getInstance(currentUser).getDeviceDatabase();
        this.databaseManager = DatabaseManager.getInstance(currentUser);

        addDatabaseListener();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (DLog.DEBUG) {
            DLog.d(TAG, "onDestroy() called");
        }
        if (deviceDatabase != null) {
            deviceDatabase.removeEventListener(deviceStatusListener);
        }
        if (connectBluetoothTask != null) {
            ConnectBluetoothTask task = connectBluetoothTask.get();
            if (task != null) {
                task.cancel(true);
            }
        }
        disconnect();
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    private void addDatabaseListener() {
        deviceStatusListener = new SimpleChildEventListener() {
            @Override
            public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                try {
                    if (DLog.DEBUG) {
                        DLog.d(TAG, "onChildChanged() called with: dataSnapshot = ["
                                + dataSnapshot + "], s = [" + s + "]");
                    }
                    ArduinoPin value = dataSnapshot.getValue(ArduinoPin.class);
                    if (value == null) {
                        return;
                    }
                    if (value.getValue() != 0) {
                        String cmd = new CommandBuilder().set().pin(value.getPin()).on().build();
                        sendCommand(cmd);
                    } else {
                        String cmd = new CommandBuilder().set().pin(value.getPin()).off().build();
                        sendCommand(cmd);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        deviceDatabase.addChildEventListener(deviceStatusListener);
    }

    @WorkerThread
    @Override
    public void onNewMessage(MessageItem message) {
        if (message.getType() == MessageItem.TYPE_INPUT_FROM_ADRUINO) {
            commandProcessor.process(message.getContent(), this);
        }
        for (IMessageListener messageListener : messageListeners) {
            messageListener.onNewMessage(message);
        }
    }

    @Override
    public void addMessageListener(@NonNull IMessageListener listener) {
        messageListeners.add(listener);
    }

    @Override
    public void removeMessageListener(@NonNull IMessageListener listener) {
        messageListeners.remove(listener);
    }

    @Override
    public void sendCommand(@NonNull String command) {
        sendCommand(command, true);
    }

    @Override
    public void connectBluetoothWith(BluetoothDevice bluetoothDevice) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "connectBluetoothWith() called with: bluetoothDevice = [" + bluetoothDevice + "]");
        }

        AppSettings.setLastConnectedDevice(this, bluetoothDevice.getAddress());

        if (isBluetoothConnected()) {
            disconnect();
        }

        ConnectBluetoothTask connectBluetoothTask = new ConnectBluetoothTask(bluetoothDevice,
                new ResultCallback<BluetoothSocket>() {
                    @Override
                    public void onSuccess(BluetoothSocket socket) {
                        onBluetoothConnected(socket);
                        for (String command : commandQueue) {
                            sendCommand(command, false);
                        }
                    }

                    @Override
                    public void onFailure(@Nullable Exception e) {
                        if (e == null) {
                            return;
                        }
                        onNewMessage(new MessageItem(MessageItem.TYPE_ERROR, e.getMessage()));
                    }
                });
        connectBluetoothTask.execute();
        this.connectBluetoothTask = new WeakReference<>(connectBluetoothTask);
    }

    private void sendCommand(String command, boolean addToQueue) {
        try {
            onNewMessage(new MessageItem(MessageItem.TYPE_OUT, command));
            if (!isBluetoothConnected()) {
                if (addToQueue) {
                    commandQueue.add(command);
                    DLog.d(TAG, "sendCommand: Bluetooth is not connected. Push command into queue");
                }
                tryToConnectWithPreviousDevice();
                return;
            }

            bluetoothCommunicator.write(command);
        } catch (IOException e) {
            e.printStackTrace();

            if (addToQueue) {
                commandQueue.add(command);
            }

            onNewMessage(new MessageItem(MessageItem.TYPE_ERROR, e.getMessage()));
            disconnect();
        } catch (Exception e) {

            onNewMessage(new MessageItem(MessageItem.TYPE_ERROR, e.getMessage()));
            e.printStackTrace();
        }
    }

    private void tryToConnectWithPreviousDevice() {
        if (DLog.DEBUG) {
            DLog.d(TAG, "tryToConnectWithPreviousDevice() called");
        }


        String address = AppSettings.getLastConnectedDevice(this);
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        Set<BluetoothDevice> bondedDevices = bluetoothAdapter.getBondedDevices();
        BluetoothDevice connectedDevice
                = bondedDevices.stream().filter(device -> device.getAddress().equals(address)).findFirst().orElse(null);
        if (connectedDevice != null) {
            this.connectBluetoothWith(connectedDevice);

            onNewMessage(new MessageItem(MessageItem.TYPE_OUT, "tryToConnectWithPreviousDevice "
                    + connectedDevice.getName() + " - " + connectedDevice.getAddress()));
        }

    }

    public boolean isBluetoothConnected() {
        return bluetoothCommunicator != null && bluetoothCommunicator.isConnected();
    }

    public void disconnect() {
        if (DLog.DEBUG) DLog.d(TAG, "disconnect() called");
        if (bluetoothCommunicator != null) {
            try {
                bluetoothCommunicator.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        bluetoothCommunicator = null;

        if (connectionListener != null) {
            connectionListener.onConnectStatusChanged(false);
        }
    }

    private void onBluetoothConnected(@NonNull BluetoothSocket socket) {
        if (connectionListener != null) {
            connectionListener.onConnectStatusChanged(socket.isConnected());
        }
        bluetoothCommunicator = new BluetoothCommunicator(socket, this);
        bluetoothCommunicator.start();
    }

    public void setConnectionListener(@Nullable IConnectListener connectionListener) {
        this.connectionListener = connectionListener;
        if (isBluetoothConnected()) {
            if (this.connectionListener != null) {
                this.connectionListener.onConnectStatusChanged(true);
            }
        }
    }

    @Override
    public void onSensorValueChanged(@NonNull SensorCondition.SensorType sensorType, double value) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "onSensorValueChanged() called with: sensorType = [" + sensorType + "], value = [" + value + "]");
        }
        sensorValues.put(sensorType.getKey(), value);

        if (databaseManager == null) {
            return;
        }

        // sync with firebase -> update UI at client
        databaseManager.pushSensorValue(sensorType, value);

        // check all configs
        for (IConfiguration config : databaseManager.getConfigs()) {
            config.check(sensorValues);
        }
    }

    public class FRemoteServiceBinder extends Binder {
        public FRemoteService getService() {
            return FRemoteService.this;
        }
    }

}
