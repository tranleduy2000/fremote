package com.duy.iremote.server.command;

public interface ICommandProcessor {
    void process(String command, ICommandListener commandListener);
}
