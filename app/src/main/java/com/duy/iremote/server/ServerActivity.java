package com.duy.iremote.server;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.WorkerThread;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.duy.iremote.AppMode;
import com.duy.iremote.R;
import com.duy.iremote.client.dashboard.DashboardActivity;
import com.duy.iremote.server.services.FRemoteService;
import com.duy.iremote.server.services.IConnectListener;
import com.duy.iremote.server.services.IMessageListener;
import com.duy.iremote.shared.database.DatabaseManager;
import com.duy.iremote.shared.helper.AppSettings;
import com.duy.iremote.shared.login.LoginActivity;
import com.duy.iremote.shared.utils.DLog;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;

/**
 * We need connect bluetooth here and start service
 */
public class ServerActivity extends AppCompatActivity implements IMessageListener, IConnectListener {

    private static final String TAG = "MessageActivity";
    private static final int RC_ENABLE_BLUETOOTH = 102;

    private final Handler handler = new Handler();

    private MenuItem connectButton;
    private EditText commandEditText;
    private RecyclerView listMessageView;
    private MessageAdapter messageAdapter;
    @Nullable
    private FRemoteService remoteService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            if (service instanceof FRemoteService.FRemoteServiceBinder) {
                remoteService = ((FRemoteService.FRemoteServiceBinder) service).getService();
                ServerActivity.this.onServiceConnected();
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle(R.string.server_mode);

        initView();

        bindRemoteService();
    }

    @Override
    protected void onDestroy() {
        if (remoteService != null) {
            remoteService.removeMessageListener(this);
        }
        unbindService(serviceConnection);
        super.onDestroy();
    }

    private void initView() {
        listMessageView = findViewById(R.id.message_list);
        LinearLayoutManager layout = new LinearLayoutManager(this);
        layout.setStackFromEnd(true);
        listMessageView.setLayoutManager(layout);
        messageAdapter = new MessageAdapter(this);
        listMessageView.setAdapter(messageAdapter);

        commandEditText = findViewById(R.id.edit_command);
        findViewById(R.id.btn_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (remoteService != null) {
                    remoteService.sendCommand(commandEditText.getText().toString());
                }
            }
        });
    }

    private void bindRemoteService() {
        Intent intent = new Intent(this, FRemoteService.class);
        startService(intent);
        bindService(intent, serviceConnection, BIND_AUTO_CREATE);
    }

    private void onServiceConnected() {
        if (remoteService == null) {
            if (DLog.DEBUG) {
                DLog.d(TAG, "populateMessageView: Service is not connected");
            }
            return;
        }
        remoteService.addMessageListener(this);
        remoteService.setConnectionListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_ENABLE_BLUETOOTH) {
            if (resultCode == RESULT_OK) {
                connectBluetooth();
            }
        }
    }

    private void connectBluetooth() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.message_bluetooth_not_avaiable), Toast.LENGTH_LONG).show();
            return;
        }
        if (!bluetoothAdapter.isEnabled()) {
            startActivityForResult(new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE), RC_ENABLE_BLUETOOTH);
            return;
        }

        final int size = bluetoothAdapter.getBondedDevices().size();
        final BluetoothDevice[] bondedDevices = bluetoothAdapter.getBondedDevices().toArray(new BluetoothDevice[size]);
        final String[] deviceNames = Arrays.stream(bondedDevices)
                .map(device -> device.getName() + " - " + device.getAddress()).toArray(String[]::new);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.select_device);
        builder.setSingleChoiceItems(deviceNames, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                BluetoothDevice toBeConnect = bondedDevices[which];
                if (DLog.DEBUG) {
                    DLog.d(TAG, "toBeConnect = " + toBeConnect);
                }
                if (remoteService != null) {
                    remoteService.connectBluetoothWith(toBeConnect);
                }
                //update UI
                onNewMessage(new MessageItem(MessageItem.TYPE_OUT, "Connecting with device " + toBeConnect.getName()));
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    @WorkerThread
    @Override
    public void onNewMessage(final MessageItem message) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "onNewMessage() called with: message = [" + message + "]");
        }
        handler.post(new Runnable() {
            @Override
            public void run() {
                if (messageAdapter != null) {
                    messageAdapter.add(message);
                    listMessageView.scrollToPosition(messageAdapter.getItemCount() - 1);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_command, menu);
        connectButton = menu.findItem(R.id.action_toggle_connect);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_toggle_connect:
                if (remoteService != null) {
                    if (remoteService.isBluetoothConnected()) {
                        remoteService.disconnect();
                    } else {
                        connectBluetooth();
                    }
                }
                break;
            case R.id.action_sign_out:
                signOut();
                break;

            case R.id.action_dashboard:
                AppSettings.setMode(this, AppMode.CLIENT);
                startActivity(new Intent(this, DashboardActivity.class));
                finish();
                break;

            case R.id.action_clear_log:
                if (messageAdapter != null) {
                    messageAdapter.clear();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void signOut() {
        unbindService(serviceConnection);
        Intent intent = new Intent(this, FRemoteService.class);
        stopService(intent);

        DatabaseManager.dispose();
        FirebaseAuth.getInstance().signOut();
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        GoogleSignIn.getClient(this, googleSignInOptions)
                .signOut()
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        startActivity(new Intent(ServerActivity.this, LoginActivity.class));
                        finish();
                    }
                });
    }

    @Override
    public void onConnectStatusChanged(boolean isConnected) {
        if (DLog.DEBUG) {
            DLog.d(TAG, "onConnectStatusChanged() called with: isConnected = [" + isConnected + "]");
        }
        if (isConnected) {
            connectButton.setTitle(R.string.disconnect);
        } else {
            connectButton.setTitle(R.string.connect);
        }

        onNewMessage(new MessageItem(MessageItem.TYPE_OUT,
                "onConnectStatusChanged: " + (isConnected ? "connected" : "disconnected")));
    }
}
