package com.duy.iremote.server.services;

import android.support.annotation.WorkerThread;

import com.duy.iremote.server.MessageItem;


public interface IMessageListener {
    @WorkerThread
    void onNewMessage(MessageItem message);
}
