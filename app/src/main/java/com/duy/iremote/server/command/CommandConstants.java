package com.duy.iremote.server.command;

public class CommandConstants {
    public static final String HIGH = "1";
    public static final String LOW = "0";

    public static final String SET = "SET";
    public static final String GET = "GET";
    public static final String VALUE = "VAL";
}
