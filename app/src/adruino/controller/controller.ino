#define DEBUG false
#define bluetooth Serial1

const String SET_CMD = "SET";  // SET PIN VAL
const String GET_CMD = "GET"; // GET PIN
const String VAL_CMD = "VAL";
const char ARG_DELIMITER = ' ';
const char CMD_DELIMITER = ';';
const int PIN_TEMP_SENSOR = A0;

String commandBuffer = "";
String lastCommand = "";
unsigned long lastTimeUpdateSensorValues = 0;
const int updateSensorValueInterval = 10000;

const int relayPins[] = {38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53};

void setup() {
  // Open connections
  if (DEBUG) {
    Serial.begin(9600);
    Serial.println("System initialing...");
    test();
  }


  if (DEBUG) Serial.println("Waiting for bluetooth connection...");
  // Bluetooth
  bluetooth.begin(9600);

  if (DEBUG) Serial.println("Turn off all relays...");
  for (int pin : relayPins) {
    pinMode(pin, OUTPUT);
    digitalWrite(pin, LOW);
    digitalWrite(pin, HIGH);
  }
  
  pinMode(PIN_TEMP_SENSOR, INPUT);
  
  if (DEBUG) Serial.println("System inited.");
}

void loop() {
  while (bluetooth.available() > 0 ) {
    char c = bluetooth.read();
    if (c == CMD_DELIMITER) {
      processCommand(commandBuffer);
      commandBuffer = "";
    } else {
      commandBuffer += c;
    }
  }

  if (millis() - lastTimeUpdateSensorValues  > updateSensorValueInterval) {
    lastTimeUpdateSensorValues = millis();
    sendSensorValues();
  }
}

void processCommand(String cmd) {
  cmd.toUpperCase();

  //  if (DEBUG) Serial.println("Received: " + cmd);
  bluetooth.println("Received: " + cmd);

  if (cmd.startsWith(SET_CMD)) { // SET PIN VAL
    // set pin val
    int pin = atoi(splitAndGet(cmd, ARG_DELIMITER, 1).c_str());
    int val = atoi(splitAndGet(cmd, ARG_DELIMITER, 2).c_str());
    // pin == 0 means number format exception occured
    if (pin != 0) digitalWrite(pin, val);
  } else if (cmd.startsWith(GET_CMD)) {
    // get pin
    int pin = atoi(splitAndGet(cmd, ARG_DELIMITER, 1).c_str());
    int val = digitalRead(pin);
    if (pin != 0) sendCommand(VAL_CMD + ARG_DELIMITER + String(pin) + ARG_DELIMITER + String(val));
  }
}

void sendSensorValues() {
  int lightValue = readLightSensorValue();
  sendCommand(VAL_CMD + ARG_DELIMITER + "light" + ARG_DELIMITER + String(lightValue));

  int tempValue = readTemperatureSensorValue();
  sendCommand(VAL_CMD + ARG_DELIMITER + "temperature" + ARG_DELIMITER + String(tempValue));

  int humidityValue = readHumiditySensorValue();
  sendCommand(VAL_CMD + ARG_DELIMITER + "humidity" + ARG_DELIMITER + String(humidityValue));
}

// Helper methods

int readLightSensorValue() {
  return 30;
}

int readTemperatureSensorValue() {
  return analogRead(PIN_TEMP_SENSOR);
}

int readHumiditySensorValue() {
  return 30;
}

void sendCommand(String cmd) {
  //  if (DEBUG) Serial.println("Send command: " + cmd);
  lastCommand = cmd;
  bluetooth.println(cmd);
}


String splitAndGet(String src, char delimiter, int index) {
  src = " " + src;
  //  Serial.println(src);
  int currentPart = -1;
  //  Serial.println(currentPart);
  int i;
  for (i = 0; i < src.length(); i++) {
    if (src.charAt(i) == delimiter) {
      currentPart += 1;
      //       Serial.println("currentPart " + String(currentPart));
      //       Serial.println("i " + String(i));
      if (currentPart == index) {
        break;
      }
    }
  }
  //  i = the index of space
  i++;
  //  Serial.println("i++ " + String(i));
  String res = "";
  while (i < src.length() && src.charAt(i) != delimiter) {
    res += src.charAt(i);
    //    Serial.println("res " + res);
    i++;
  }
  return res;
}

void test() {
  Serial.println("Testing code");

  Serial.println("# Testing splitAndGet() method");
  String cmd = "a b c d";
  assertEquals(splitAndGet(cmd, ARG_DELIMITER, 0), "a");
  assertEquals(splitAndGet(cmd, ARG_DELIMITER, 1), "b");
  assertEquals(splitAndGet(cmd, ARG_DELIMITER, 2), "c");
  assertEquals(splitAndGet(cmd, ARG_DELIMITER, 3), "d");

  Serial.println("# Testing \"set\" command");
  pinMode(relayPins[0], OUTPUT);
  digitalWrite(relayPins[0], LOW);
  assertEquals(digitalRead(relayPins[0]), LOW);

  cmd = SET_CMD + ARG_DELIMITER + String(relayPins[0]) + ARG_DELIMITER + String(HIGH);
  assertEquals(cmd, "SET 38 1");
  processCommand(cmd);
  assertEquals(digitalRead(relayPins[0]), HIGH);

  Serial.println("# Testing \"get\" command");
  processCommand(GET_CMD + ARG_DELIMITER + String(relayPins[0]));
  assertEquals(lastCommand, "VAL 38 1");
  digitalWrite(relayPins[0], LOW);
  processCommand(GET_CMD + ARG_DELIMITER + String(relayPins[0]));
  assertEquals(lastCommand, "VAL 38 0");

  Serial.println("# Testing catch exception");
  processCommand("SET S T");
  processCommand("SET 1 A");
  processCommand("GET LL");
  assertEquals(1, 1);
}

void assertEquals(String actual, String expected) {
  if (expected == actual) {
    Serial.println("  Test succeeded.");
  } else {
    Serial.println("! Test failed. Expected '" + expected + "' but found '" + actual + "'");
  }
}
void assertEquals(int actual, int expected) {
  if (expected == actual) {
    Serial.println("  Test succeeded.");
  } else {
    Serial.println("! Test failed. Expected '" + String(expected) + "' but found '" + String(actual) + "'");
  }
}
