package com.duy.iremote.shared.config;

import com.duy.iremote.shared.config.condition.SensorCondition;
import com.duy.iremote.shared.config.condition.RelationOperatorCondition;

import junit.framework.TestCase;

import java.util.HashMap;

public class ConfigurationTest extends TestCase {

    public void test1() {
        Configuration configuration = new Configuration();
        configuration.setIdentifier(23123);
        configuration.addCondition(new SensorCondition(SensorCondition.SensorType.LIGHT,
                RelationOperatorCondition.RelationOperator.GREATER, 30));
        configuration.addCondition(new SensorCondition(SensorCondition.SensorType.LIGHT,
                RelationOperatorCondition.RelationOperator.LESS, 40));

        assertEquals(configuration.toMap().toString(),
                "{id=23123, conditions=[{type=sensor_condition, value=30.0, operator=GREATER, sensor_name=LIGHT}, {type=sensor_condition, value=40.0, operator=LESS, sensor_name=LIGHT}]}");

        HashMap<String, Object> properties = configuration.toMap();
        Configuration newConfiguration = new ConfigurationParser().parse(properties);
        assertEquals(newConfiguration, configuration);
    }

    public void test2() {
        Configuration configuration = new Configuration();
        configuration.setName("Name");
        configuration.setIdentifier(23123);
        configuration.addCondition(new SensorCondition(SensorCondition.SensorType.LIGHT,
                RelationOperatorCondition.RelationOperator.GREATER, 30));
        configuration.addCondition(new SensorCondition(SensorCondition.SensorType.LIGHT,
                RelationOperatorCondition.RelationOperator.LESS, 40));

        assertEquals(configuration.toMap().toString(),
                "{name=Name, id=23123, conditions=[{type=sensor_condition, value=30.0, operator=GREATER, sensor_name=LIGHT}, {type=sensor_condition, value=40.0, operator=LESS, sensor_name=LIGHT}]}");

        HashMap<String, Object> properties = configuration.toMap();
        Configuration newConfiguration = new ConfigurationParser().parse(properties);
        assertEquals(newConfiguration, configuration);
    }
}